package com.jujuy.cmoviajojujuy.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.jujuy.cmoviajojujuy.repository.ApiBusRepository;
import com.jujuy.cmoviajojujuy.services.modelProyect.RequestBusStop;
import com.jujuy.cmoviajojujuy.services.modelProyect.ResponseBusStop;
import com.jujuy.cmoviajojujuy.services.modelPublicidades.ResponseBusPublicidades;

import java.util.Random;

public class StopBusViewModel extends ViewModel {
    public LiveData<ResponseBusStop> getStopBusLiveData(RequestBusStop requestBusStop){
        return ApiBusRepository.getInstance().getSuggestionsStopBus(requestBusStop);
    }

    public LiveData<ResponseBusPublicidades> getBusPublicidades(){
        return ApiBusRepository.getInstance().getPublicidades();
    }
}
