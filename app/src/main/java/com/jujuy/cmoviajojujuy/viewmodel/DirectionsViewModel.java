package com.jujuy.cmoviajojujuy.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.jujuy.cmoviajojujuy.repository.DirectionsRepository;
import com.jujuy.cmoviajojujuy.services.modelApiGoogle.Direction;


public class DirectionsViewModel extends ViewModel {
    public LiveData<Direction> getDirectionLiveData(String origin, String destination,String mode, String key){
        return DirectionsRepository.getInstance().getDirectionsTo(origin,destination,mode,key);
    }
    public LiveData<String[]> getPoylineLiveData(Direction direction){
        return DirectionsRepository.getInstance().getPolyline(direction);
    }

    public LiveData<Direction> getWalkDirectionTo(String origin, String destination,String mode, String key){
        return DirectionsRepository.getInstance().getWalkDirectionsTo(origin,destination,mode,key);
    }
}
