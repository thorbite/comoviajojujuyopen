package com.jujuy.cmoviajojujuy.view.ui;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.droidnet.DroidListener;
import com.droidnet.DroidNet;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.jujuy.cmoviajojujuy.R;
import com.jujuy.cmoviajojujuy.repository.ApiBusRepository;
import com.jujuy.cmoviajojujuy.services.modelProyect.Directo;
import com.jujuy.cmoviajojujuy.services.modelProyect.Indirecto;
import com.jujuy.cmoviajojujuy.services.modelProyect.RequestBusStop;
import com.jujuy.cmoviajojujuy.services.modelProyect.ResponseBusStop;
import com.jujuy.cmoviajojujuy.services.modelPublicidades.Publicidade;
import com.jujuy.cmoviajojujuy.services.modelPublicidades.ResponseBusPublicidades;
import com.jujuy.cmoviajojujuy.utils.AlerterHelper;
import com.jujuy.cmoviajojujuy.utils.BaseActivity;
import com.jujuy.cmoviajojujuy.view.adapter.DirectSuggestionRouteAdapter;
import com.jujuy.cmoviajojujuy.view.adapter.IndirectSuggestionRouteAdapter;
import com.jujuy.cmoviajojujuy.viewmodel.StopBusViewModel;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final int AUTOCOMPLETE_REQUEST_CODE = 23487;
    private static final int AUTOCOMPLETE_REQUEST_CODE_END = 12345;
    private static final int LIMITE = 50;
    private static final int RADIO = 2;

    private RecyclerView recycleViewPersistent;
    private ConstraintLayout constraintLayoutPersistent;
    private BottomSheetBehavior bottomSheetBehaviorPersistant;
    private DirectSuggestionRouteAdapter adapterSuggestionsRoutes;
    private IndirectSuggestionRouteAdapter indirectSuggestionRouteAdapter;
    private ImageView imgBtnUp;
    private ImageView imageViewInvest;
    private Button search;
    private EditText from, to;
    private Place placeFrom, placeTo;
    public Location mLastLocation;
    private LatLng latLongFrom, latLongTo;
    private StopBusViewModel stopBusViewModel;
    private List<Directo> busStopsDirect;
    private List<Indirecto> busStopsIndirect;
    private boolean isDirect = false;
    private AdView banner;
    private ImageView adView;
    private VideoView adVideoView;
    private Handler handler;
    private List<Publicidade> list;
    private DroidNet mDroidNet;
    private boolean isConnected;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mLastLocation = getIntent().getParcelableExtra("mLastLocation");
        stopBusViewModel = ViewModelProviders.of(this).get(StopBusViewModel.class);
        handler = new Handler(Looper.getMainLooper());
        inflateViews();
        setOnClickListener();
        initRecyclerViewBottomSheet();
        //loadBannerAds();
    }

    private void inflateViews() {
        imageViewInvest = findViewById(R.id.iv_invest);
        from = findViewById(R.id.edt_from);
        to = findViewById(R.id.edt_to);
        search = findViewById(R.id.button);
        imgBtnUp = findViewById(R.id.imgBtnUp);
        adView = findViewById(R.id.adView);
        adVideoView = findViewById(R.id.adVideoView);
        recycleViewPersistent = findViewById(R.id.recycleViewPersistent);
        constraintLayoutPersistent = findViewById(R.id.constraintLayoutPersistent);
        bottomSheetBehaviorPersistant = BottomSheetBehavior.from(constraintLayoutPersistent);
        bottomSheetBehaviorPersistant.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    private void setOnClickListener() {
        from.setClickable(true);
        from.setOnClickListener(view -> startAutocompleteActivity());
        to.setOnClickListener(view -> endtAutocompleteActivity());
        imageViewInvest.setOnClickListener(view -> swipeEditText());
        search.setOnClickListener(view -> searchRoute());
    }

    private void initRecyclerViewBottomSheet() {
        recycleViewPersistent.setHasFixedSize(true);
        recycleViewPersistent.setLayoutManager(new LinearLayoutManager(this));
        bottomSheetBehaviorPersistant.setState(BottomSheetBehavior.STATE_COLLAPSED);
        bottomSheetBehaviorPersistant.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {

                if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                    if (imgBtnUp.getRotation() == 0F) {
                        float deg = imgBtnUp.getRotation() + 180F;
                        imgBtnUp.animate().rotation(deg).setInterpolator(new AccelerateDecelerateInterpolator());
                    }
                }
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        if (imgBtnUp.getRotation() == 180F) {
                            float deg = 0F;
                            imgBtnUp.animate().rotation(deg).setInterpolator(new AccelerateDecelerateInterpolator());
                        }
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });
    }

    private void loadDirectSuggestionsRoute() {
        adapterSuggestionsRoutes = new DirectSuggestionRouteAdapter(MainActivity.this
                , busStopsDirect
                , new DirectSuggestionRouteAdapter.OnItemClickListener() {
            @Override
            public void onItemClickDirecto(Directo item) {
                Intent intent = new Intent(MainActivity.this, RouteActivity.class);
                intent.putExtra("placeFrom", placeFrom);
                intent.putExtra("placeTo", placeTo);
                intent.putExtra("latLongFrom", latLongFrom);
                intent.putExtra("latLongTo", latLongTo);
                intent.putExtra("itemDirecto", item);
                intent.putExtra("isDirect", isDirect);
                startActivity(intent);
            }
        });
        recycleViewPersistent.setAdapter(adapterSuggestionsRoutes);
    }

    private void loadIndirectSuggestionsRoute() {
        indirectSuggestionRouteAdapter = new IndirectSuggestionRouteAdapter(MainActivity.this
                , busStopsIndirect
                , new IndirectSuggestionRouteAdapter.OnItemClickListener() {
            @Override
            public void onItemClickIndirecto(Indirecto item) {
                Intent intent = new Intent(MainActivity.this, RouteActivity.class);
                intent.putExtra("placeFrom", placeFrom);
                intent.putExtra("placeTo", placeTo);
                intent.putExtra("latLongFrom", latLongFrom);
                intent.putExtra("latLongTo", latLongTo);
                intent.putExtra("itemIndirecto", item);
                intent.putExtra("isDirect", isDirect);
                startActivity(intent);
            }
        });
        recycleViewPersistent.setAdapter(indirectSuggestionRouteAdapter);
    }

    public void toggleBottomSheet(View view) {
        if (bottomSheetBehaviorPersistant.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            float deg = imgBtnUp.getRotation() + 180F;
            imgBtnUp.animate().rotation(deg).setInterpolator(new AccelerateDecelerateInterpolator());
            bottomSheetBehaviorPersistant.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
            bottomSheetBehaviorPersistant.setState(BottomSheetBehavior.STATE_COLLAPSED);
            float deg = 0F;
            imgBtnUp.animate().rotation(deg).setInterpolator(new AccelerateDecelerateInterpolator());
        }
    }

    private void searchRoute() {
        from.setError(null);
        to.setError(null);
        if (from.getText().toString().equals("")) {
            from.setFocusable(true);
            from.setError("Por favor ingrese un origen");
            from.requestFocus();
        } else if (to.getText().toString().equals("")) {
            to.setFocusable(true);
            to.setError("Por favor ingrese un destino");
            to.requestFocus();
        } else {
            initProgressBar();
            loadLiveData();
        }
    }

    public void loadLiveData() {
        stopBusViewModel.getStopBusLiveData(new RequestBusStop(String.valueOf(latLongFrom.latitude)
                , String.valueOf(latLongFrom.longitude)
                , String.valueOf(latLongTo.latitude)
                , String.valueOf(latLongTo.longitude), LIMITE, RADIO))
                .observe(MainActivity.this, new Observer<ResponseBusStop>() {
                    @Override
                    public void onChanged(ResponseBusStop responseBusStop) {
                        dismissProgressBar();
                        if (responseBusStop != null) {
                            if (responseBusStop.getListBusStop() != null) {
                                if (responseBusStop.getListBusStop().getDirectBusStops().size() > 0) {
                                    isDirect = true;
                                    busStopsDirect = responseBusStop.getListBusStop().getDirectBusStops();
                                    loadDirectSuggestionsRoute();
                                    adapterSuggestionsRoutes.setData(busStopsDirect);

                                } else {
                                    isDirect = false;
                                    busStopsIndirect = responseBusStop.getListBusStop().getIndirectBusStops();
                                    loadIndirectSuggestionsRoute();
                                    indirectSuggestionRouteAdapter.setData(busStopsIndirect);
                                }
                                bottomSheetBehaviorPersistant.setState(BottomSheetBehavior.STATE_EXPANDED);
                                int peekHeightPx = getResources().getDimensionPixelSize(R.dimen.peek_height);
                                bottomSheetBehaviorPersistant.setPeekHeight(peekHeightPx);
                            } else
                                AlerterHelper.showInfo(MainActivity.this, responseBusStop.getMessage());
                        } else {
                            AlerterHelper.showError(MainActivity.this, "No tienes conexion a internet");
                        }
                    }
                });
    }

    private void swipeEditText() {
        String tempFrom = from.getText().toString();
        String tempTo = to.getText().toString();
        from.setText(tempTo);
        to.setText(tempFrom);
        Place temp1 = placeFrom;
        placeFrom = placeTo;
        placeTo = temp1;

        LatLng temp = latLongFrom;
        latLongFrom = latLongTo;
        latLongTo = temp;

        if (imageViewInvest.getRotation() == 0F) {
            float deg = imageViewInvest.getRotation() + 180F;
            imageViewInvest.animate().rotation(deg).setInterpolator(new AccelerateDecelerateInterpolator());
        } else {
            float deg = 0F;
            imageViewInvest.animate().rotation(deg).setInterpolator(new AccelerateDecelerateInterpolator());

        }
        hideBottomSheet();
        searchRoute();
    }

    private void endtAutocompleteActivity() {
        Intent intent = new Intent(this, SearchPlaceActivity.class);
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE_END);
    }

    private void hideBottomSheet(){
        bottomSheetBehaviorPersistant.setPeekHeight(0);
        bottomSheetBehaviorPersistant.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent intent) {
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                placeFrom = intent.getParcelableExtra("place");
                hideBottomSheet();
                if (placeFrom != null) {
                    String place1 = placeFrom.getName();
                    from.setText(place1);
                    latLongFrom = new LatLng(placeFrom.getLatLng().latitude, placeFrom.getLatLng().longitude);
                } else {
                    from.setText("Ubicacion actual");
                    if (mLastLocation == null) {
                        Location temp = intent.getParcelableExtra("mLastLocation");
                        latLongFrom = new LatLng(temp.getLatitude(), temp.getLongitude());
                    } else {
                        latLongFrom = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                    }
                }
            }
        }
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE_END) {
            if (resultCode == Activity.RESULT_OK) {
                placeTo = intent.getParcelableExtra("place");
                hideBottomSheet();
                if (placeTo != null) {
                    String place1 = placeTo.getName();
                    to.setText(place1);
                    latLongTo = new LatLng(placeTo.getLatLng().latitude, placeTo.getLatLng().longitude);
                } else {
                    to.setText("Ubicacion actual");
                    if (mLastLocation == null) {
                        Location temp = intent.getParcelableExtra("mLastLocation");
                        latLongTo = new LatLng(temp.getLatitude(), temp.getLongitude());
                    } else {
                        latLongTo = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                    }
                }
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
        }
        super.onActivityResult(requestCode, resultCode, intent);
    }

    private void startAutocompleteActivity() {
        Intent intent = new Intent(this, SearchPlaceActivity.class);
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
    }


    private void loadBannerAds() {
        stopBusViewModel.getBusPublicidades().observe(MainActivity.this, new Observer<ResponseBusPublicidades>() {
            @Override
            public void onChanged(ResponseBusPublicidades responseBusPublicidades) {
                if (responseBusPublicidades != null) {
                    int arraySize = responseBusPublicidades.getData().getPublicidades().size();
                    list = responseBusPublicidades.getData().getPublicidades();

                    Handler handler = new Handler();
                    Runnable runnable = new Runnable() {
                        int i = 0;
                        public void run() {
                            Glide.with(getApplicationContext())
                                    .load("http://45.132.240.28" + list.get(i).getRutaArchivo())
                                    .fitCenter()
                                    .into(adView);
                            i++;
                            if (i > arraySize - 1) {
                                i = 0;
                            }
                            handler.postDelayed(this, list.get(i).getDuracionPublicidad() * 1000);
                        }
                    };
                    handler.postDelayed(runnable, 200);

                    /*for (int i = 0; i < arraySize; i++) {
                        if (list.get(i).getTipoArchivo().equals("image/png")) {
                            adVideoView.setVisibility(View.GONE);

                            Glide.with(getApplicationContext())
                                    .load("http://www.noahtec.xyz" + list.get(i).getRutaArchivo())
                                    .fitCenter()
                                    .into(adView);
                            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(MainActivity.this, "probando el handler", Toast.LENGTH_SHORT).show();
                                }
                            }, list.get(i).getDuracionPublicidad() * 1000);
                        } else if (list.get(i).getTipoArchivo().equals("video/mp4")){
                            adVideoView.setVisibility(View.VISIBLE);
                            adVideoView.setVideoPath("http://www.noahtec.xyz" + list.get(i).getRutaArchivo());
                            adVideoView.start();
                            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    adVideoView.stopPlayback();
                                }
                            }, list.get(i).getDuracionPublicidad() * 1000);
                        }
                    }*/
                }

            }
        });
    }

    public void initProgressBar() {
        ProgressDialogFragment.newInstance("I AM TITLE!!").show(getSupportFragmentManager(), "ProgressDialog");
    }

    public void dismissProgressBar() {
        FragmentManager manager = getSupportFragmentManager();
        Fragment prev = manager.findFragmentByTag("ProgressDialog");
        if (prev != null) {
            manager.beginTransaction().remove(prev).commit();
        }
    }

}
