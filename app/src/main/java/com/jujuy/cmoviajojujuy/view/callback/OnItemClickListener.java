package com.jujuy.cmoviajojujuy.view.callback;

import com.jujuy.cmoviajojujuy.services.modelProyect.Directo;
import com.jujuy.cmoviajojujuy.services.modelProyect.Indirecto;

public interface OnItemClickListener {
    void onItemClickDirect(Directo item);
    void onItemClickIndirect(Indirecto item);
}