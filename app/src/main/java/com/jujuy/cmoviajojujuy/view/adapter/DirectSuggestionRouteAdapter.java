package com.jujuy.cmoviajojujuy.view.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.jujuy.cmoviajojujuy.R;
import com.jujuy.cmoviajojujuy.services.modelApiGoogle.Direction;
import com.jujuy.cmoviajojujuy.services.modelProyect.Directo;
import com.jujuy.cmoviajojujuy.services.modelProyect.Indirecto;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import static com.jujuy.cmoviajojujuy.view.adapter.IndirectSuggestionRouteAdapter.TYPE_FIRST_ITEM;
import static com.jujuy.cmoviajojujuy.view.adapter.IndirectSuggestionRouteAdapter.TYPE_ITEM;

public class DirectSuggestionRouteAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private List<Directo> listBusStops;
    private OnItemClickListener listener;
    private Context context;

    public DirectSuggestionRouteAdapter(Context context, List<Directo> list, OnItemClickListener listener){
        this.context=context;
        this.listBusStops =list;
        this.listener=listener;
    }

    final class FirstViewHolder extends RecyclerView.ViewHolder {
        private TextView txtLinea,txtDireccion,closestRoute;
        public FirstViewHolder(View itemView) {
            super(itemView);
            txtLinea=itemView.findViewById(R.id.tv_linea_colectivo);
            txtDireccion=itemView.findViewById(R.id.tv_direccion);
            closestRoute = itemView.findViewById(R.id.tv_closets_route);
        }
        public void bind(Directo item, final OnItemClickListener listener) {
            txtLinea.setText(item.getLinea().getLinea() + " " + item.getLinea().getEmpresa());
            txtDireccion.setText(item.getLinea().getLineaDescripcion());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClickDirecto(item);
                }
            });
        }
    }

    final class NormalViewHolder extends RecyclerView.ViewHolder {
        private TextView txtLinea,txtDireccion,closestRoute;
        public NormalViewHolder(View itemView) {
            super(itemView);
            txtLinea=itemView.findViewById(R.id.edt_linea_colectivo);
            txtDireccion=itemView.findViewById(R.id.edt_direccion);
            closestRoute = itemView.findViewById(R.id.label_closets_route);
        }
        public void bind(Directo item, final OnItemClickListener listener) {
            txtLinea.setText(item.getLinea().getLinea() + " " + item.getLinea().getEmpresa());
            txtDireccion.setText(item.getLinea().getLineaDescripcion());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClickDirecto(item);
                }
            });
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_FIRST_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_close_route, parent, false);
            return new FirstViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_suggestions_route, parent, false);
            return new NormalViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(@NotNull final RecyclerView.ViewHolder holder, final int position) {
        if(holder.getItemViewType()==TYPE_FIRST_ITEM){
            FirstViewHolder firstViewHolder = (FirstViewHolder) holder;
            firstViewHolder.bind(listBusStops.get(position), listener);
        }else{
            NormalViewHolder normalViewHolder = (NormalViewHolder) holder;
            normalViewHolder.bind(listBusStops.get(position), listener);
        }
    }

    public void setData(List<Directo> stops){
        this.listBusStops = stops;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if(listBusStops != null){
            return listBusStops.size();
        }else return 0;
    }

    public interface OnItemClickListener {
        void onItemClickDirecto(Directo item);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) return TYPE_FIRST_ITEM;
        else return TYPE_ITEM;
    }

}

