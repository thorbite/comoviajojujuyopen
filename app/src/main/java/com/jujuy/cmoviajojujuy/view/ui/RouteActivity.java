package com.jujuy.cmoviajojujuy.view.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.libraries.places.api.model.Place;
import com.google.maps.android.PolyUtil;
import com.jujuy.cmoviajojujuy.R;
import com.jujuy.cmoviajojujuy.services.modelApiGoogle.Direction;
import com.jujuy.cmoviajojujuy.services.modelApiGoogle.Step;
import com.jujuy.cmoviajojujuy.services.modelProyect.Directo;
import com.jujuy.cmoviajojujuy.services.modelProyect.Indirecto;
import com.jujuy.cmoviajojujuy.utils.AlerterHelper;
import com.jujuy.cmoviajojujuy.utils.AnchorSheetBehavior;
import com.jujuy.cmoviajojujuy.utils.BaseActivity;
import com.jujuy.cmoviajojujuy.utils.DateUtil;
import com.jujuy.cmoviajojujuy.utils.DrawPathBus;
import com.jujuy.cmoviajojujuy.utils.DrawPathWalk;
import com.jujuy.cmoviajojujuy.utils.MapHelper;
import com.jujuy.cmoviajojujuy.viewmodel.DirectionsViewModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.jujuy.cmoviajojujuy.utils.Converters.getMarkerIconFromDrawable;
import static com.jujuy.cmoviajojujuy.utils.DirectionsHelper.getAddress;
import static com.jujuy.cmoviajojujuy.utils.DirectionsHelper.getPolylineWalkToStopBus;
import static com.jujuy.cmoviajojujuy.utils.DrawPathWalk.PATTERN_DOTTED_LINE;

public class RouteActivity extends BaseActivity implements OnMapReadyCallback {

    private static final String TAG = "RouteActivity";

    private AnchorSheetBehavior bsBehavior;
    RelativeLayout tapactionlayout;
    private FrameLayout lineTripBus;
    LinearLayout bottomSheet, containerTakeAnotherBus, container_walk_another_stop_bus;
    DirectionsViewModel directionsViewModel;
    private TextView txtRoute;
    private ImageView imgButtonUpDown;
    private Toolbar toolbar;
    //bottomsheet
    private TextView fromLocationTxt, toLocationTxt, takeBus, getOffBus, numberBusDirect, walkToStopBus, walkToDestiny, timeInitTravel, timeArrival, takeAnotherBus, getOffAnotherBus, numberBusIndirect;
    //var google
    GoogleMap mMap;
    private Polyline[] polylineArray, polylineWalkToStopBus, polylineWalkToDestiny;
    private List<LatLng> latLngs;
    private Place placeFrom, placeTo;
    private LatLng latLngFrom, latLngTo;
    private boolean isDirect;
    private Directo directo;
    private Indirecto indirecto;
    private TextView walkToBusTransfer;

    private int durationTotal = 0;
    private Marker markerOrigin;
    private Marker markerDestiny;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        directionsViewModel = ViewModelProviders.of(this).get(DirectionsViewModel.class);

        inflateViews();
        getIntentsExtras();
        setUpBottomSheetBehavior();
    }

    private void inflateViews() {
        tapactionlayout = findViewById(R.id.tap_action_layout);
        bottomSheet = findViewById(R.id.bottomsheet_map);
        toolbar = findViewById(R.id.toolbar_route);
        txtRoute = findViewById(R.id.txt_route);
        imgButtonUpDown = findViewById(R.id.arrow_up_down);
        fromLocationTxt = findViewById(R.id.from_location_name);
        toLocationTxt = findViewById(R.id.destinty_name);
        takeBus = findViewById(R.id.take_bus);
        getOffBus = findViewById(R.id.get_off_bus);
        numberBusDirect = findViewById(R.id.number_bus_direct);
        walkToStopBus = findViewById(R.id.walk_to_stop_bus);
        walkToDestiny = findViewById(R.id.walk_to_destiny);
        timeInitTravel = findViewById(R.id.time_init_travel);
        timeArrival = findViewById(R.id.time_arrival);
        lineTripBus = findViewById(R.id.line_trip_bus);
        walkToBusTransfer = findViewById(R.id.walk_another_stop_bus);

        takeAnotherBus = findViewById(R.id.take_another_bus);
        getOffAnotherBus = findViewById(R.id.get_off_another_bus);
        numberBusIndirect = findViewById(R.id.number_bus_indirect);
        containerTakeAnotherBus = findViewById(R.id.container_take_another_bus);
        container_walk_another_stop_bus = findViewById(R.id.container_walk_another_stop_bus);

        imgButtonUpDown.setOnClickListener(view -> onClickArrowButton());
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void checkItIntents(){
        latLngs = new ArrayList<LatLng>();
        if (!(placeFrom == null) && !(placeTo == null)) {
            latLngs.add(new LatLng(placeFrom.getLatLng().latitude, placeFrom.getLatLng().longitude));
            latLngs.add(new LatLng(placeTo.getLatLng().latitude, placeTo.getLatLng().longitude));
            txtRoute.setText(placeFrom.getName() + " a " + placeTo.getName());
            fromLocationTxt.setText(placeFrom.getName());
            toLocationTxt.setText(placeTo.getName());
        } else if ((placeFrom == null) && !(placeTo == null)) {
            latLngs.add(latLngFrom);
            latLngs.add(new LatLng(placeTo.getLatLng().latitude, placeTo.getLatLng().longitude));
            txtRoute.setText("Ubicacion actual a " + placeTo.getName());
            fromLocationTxt.setText("Tu ubicacion actual");
            toLocationTxt.setText(placeTo.getName());
        } else if (!(placeFrom == null) && (placeTo == null)) {
            latLngs.add(new LatLng(placeFrom.getLatLng().latitude, placeFrom.getLatLng().longitude));
            latLngs.add(latLngTo);
            txtRoute.setText(placeFrom.getName() + " a Ubicacion actual");
            fromLocationTxt.setText(placeFrom.getName());
            toLocationTxt.setText("Tu ubicacion actual");
        }

        if (isDirect) {
            loadDirectUI();
        } else loadIndirectUI();

    }

    private void getIntentsExtras() {
        placeFrom = getIntent().getParcelableExtra("placeFrom");
        placeTo = getIntent().getParcelableExtra("placeTo");
        latLngFrom = getIntent().getParcelableExtra("latLongFrom");
        latLngTo = getIntent().getParcelableExtra("latLongTo");
        directo = (Directo) getIntent().getSerializableExtra("itemDirecto");
        indirecto = (Indirecto) getIntent().getSerializableExtra("itemIndirecto");
        isDirect = getIntent().getBooleanExtra("isDirect", true);

    }

    private void onClickArrowButton() {
        if (bsBehavior.getState() != AnchorSheetBehavior.STATE_EXPANDED) {
            float deg = imgButtonUpDown.getRotation() + 180F;
            imgButtonUpDown.animate().rotation(deg).setInterpolator(new AccelerateDecelerateInterpolator());
            bsBehavior.setState(AnchorSheetBehavior.STATE_EXPANDED);
        } else {
            bsBehavior.setState(AnchorSheetBehavior.STATE_COLLAPSED);
            float deg = 0F;
            imgButtonUpDown.animate().rotation(deg).setInterpolator(new AccelerateDecelerateInterpolator());
        }
        if (bsBehavior.getState() == AnchorSheetBehavior.STATE_SETTLING) {
            float deg = 180f;
            imgButtonUpDown.animate().rotation(deg).setInterpolator(new AccelerateDecelerateInterpolator());
        }
    }


    private void setUpBottomSheetBehavior() {
        bsBehavior = AnchorSheetBehavior.from(bottomSheet);
        bsBehavior.setState(AnchorSheetBehavior.STATE_ANCHOR);

        //anchor offset. any value between 0 and 1 depending upon the position u want
        bsBehavior.setAnchorOffset(0.5f);
        bsBehavior.setAnchorSheetCallback(new AnchorSheetBehavior.AnchorSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == AnchorSheetBehavior.STATE_COLLAPSED) {
                    //action if needed
                }

                if (newState == AnchorSheetBehavior.STATE_EXPANDED) {

                }

                if (newState == AnchorSheetBehavior.STATE_DRAGGING) {

                }

                if (newState == AnchorSheetBehavior.STATE_ANCHOR) {

                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                imgButtonUpDown.setRotation(slideOffset * 180);
                float h = bottomSheet.getHeight();
                float off = h * slideOffset;

                switch (bsBehavior.getState()) {
                    case AnchorSheetBehavior.STATE_DRAGGING:
                        setMapPaddingBotttom(off);
                        if (latLngs.size() >= 2) {
                            MapHelper.centerLatLngsInMap(mMap, latLngs, false);
                        }
                        break;
                    case AnchorSheetBehavior.STATE_SETTLING:
                        setMapPaddingBotttom(off);
                        if (latLngs.size() >= 2) {
                            MapHelper.centerLatLngsInMap(mMap, latLngs, false);
                        }
                        //if (mLoc != null) mMap.moveCamera(CameraUpdateFactory.newLatLng(mLoc));
                        break;
                    case AnchorSheetBehavior.STATE_HIDDEN:
                        break;
                    case AnchorSheetBehavior.STATE_EXPANDED:
                        setMapPaddingBotttom(off);
                        if (latLngs.size() >= 2) {
                            MapHelper.centerLatLngsInMap(mMap, latLngs, false);
                        }
                        break;
                    case AnchorSheetBehavior.STATE_COLLAPSED:
                        break;
                }

            }
        });

        tapactionlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bsBehavior.getState() == AnchorSheetBehavior.STATE_COLLAPSED) {
                    bsBehavior.setState(AnchorSheetBehavior.STATE_ANCHOR);
                }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);
        addMarkerEndTrip();
        checkItIntents();
        setTimeArrival();
        //addMarkerWalk();
        if (ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION
                    }, 0);
            return;
        }


        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                setmMap();
                if (!isDirect) {
                    drawPoly();
                } else drawPolyDirect();
            }
        });
    }

    private void setMapPaddingBotttom(Float offset) {
        Float maxMapPaddingBottom = 1.0f;
        mMap.setPadding(0, 0, 0, Math.round(offset * maxMapPaddingBottom));
    }

    private void setmMap() {

        /*mMap.setPadding(0, bottomSheet.getHeight() / 4, 0, bottomSheet.getHeight());
        MapHelper.centerLatLngsInMap(mMap, latLngs, false);*/
        setMapPaddingBotttom(Float.parseFloat(String.valueOf(bsBehavior.getAnchorOffset())));
        if (latLngs.size() >= 2) {
            MapHelper.centerLatLngsInMap(mMap, latLngs, false);
        }

        LatLng latlng = new LatLng(latLngFrom.latitude, latLngFrom.longitude);
        markerOrigin = mMap.addMarker(new MarkerOptions()
                .position(latlng)
                .title("Origen")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
        markerOrigin.showInfoWindow();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 0:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    onMapReady(mMap);
                }
        }
    }

    public void drawRoutePolyline(LatLng from, LatLng to, String mode, int color) {
        Drawable circleDrawable = getResources().getDrawable(R.drawable.leg_circle_middle);
        circleDrawable.mutate().setColorFilter(color, PorterDuff.Mode.SRC_IN);
        BitmapDescriptor markerIcon = getMarkerIconFromDrawable(circleDrawable);
        mMap.addMarker(new MarkerOptions().position(from).icon(markerIcon));
        directionsViewModel.getDirectionLiveData(from.latitude + "," + from.longitude
                , to.latitude + "," + to.longitude, mode,
                getString(R.string.google_maps_key))
                .observe(this, direction -> {
                    directionsViewModel.getPoylineLiveData(direction).observe(this,
                            polylines -> {

                                if (polylines != null) {
                                    //new DrawPathBus(mMap, polylines, color).execute();
                                    PolylineOptions options = new PolylineOptions();
                                    options.color(color);
                                    options.width(10);
                                    options.geodesic(true);
                                    for (String s : polylines) {
                                        options.addAll(PolyUtil.decode(s));
                                    }
                                    mMap.addPolyline(options);
                                }
                                if (direction.getRoutes().size() > 0) {
                                    durationTotal += direction.getRoutes().get(0).getLegs().get(0).getDuration().getValue();
                                }
                                setTimeArrival();
                            });
                });
    }

    private void drawPolylineWalking(LatLng from, LatLng to, TextView textView) {
        int color = getResources().getColor(R.color.gray_white);
        directionsViewModel.getWalkDirectionTo(from.latitude + "," + from.longitude
                , to.latitude + "," + to.longitude, "walking"
                , getString(R.string.google_maps_key)).observe(this, direction -> {
            directionsViewModel.getPoylineLiveData(direction).observe(this,
                    polylines -> {
                        if (polylines != null) {
                            //new DrawPathWalk(mMap, polylines, color).execute();
                            PolylineOptions options = new PolylineOptions();
                            options.color(color);
                            options.width(15);
                            options.geodesic(true);
                            options.pattern(PATTERN_DOTTED_LINE);
                            for (String polyline : polylines) {
                                options.addAll(PolyUtil.decode(polyline));
                            }
                            mMap.addPolyline((PolylineOptions) options);
                        }
                        if(direction.getRoutes().size() > 0) {
                            textView.setText(getString(R.string.walking, direction.getRoutes()
                                    .get(0).getLegs().get(0).getDistance().getValue(), direction.getRoutes()
                                    .get(0).getLegs().get(0).getDuration().getText()));
                            durationTotal += direction.getRoutes().get(0).getLegs().get(0).getDuration().getValue();
                        }
                        setTimeArrival();
                    });
        });
    }

    private void drawPolylineDirectBus(List<LatLng> points, int color) {
      /*  Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
*/
        for (int i = 0; i < points.size() - 1; i++) {
            LatLng src = points.get(i);
            LatLng dest = points.get(i + 1);

            drawRoutePolyline(src, dest, "driving", color);
        }
    }

    private void walkingToGetBus() {
        LatLng from = latLngs.get(0);
        LatLng to = new LatLng(Double.parseDouble(directo.getLinea().getRecorrido().get(0).getLatitud()), Double.parseDouble(directo.getLinea().getRecorrido().get(0).getLongitud()));
        drawPolylineWalking(from, to, walkToStopBus);
    }

    private void walkingToDestiny() {
        LatLng from = new LatLng(Double.parseDouble(directo.getLinea().getRecorrido().get(directo.getLinea().getRecorrido().size() - 1).getLatitud()), Double.parseDouble(directo.getLinea().getRecorrido().get(directo.getLinea().getRecorrido().size() - 1).getLongitud()));
        LatLng to = latLngs.get(1);
        drawPolylineWalking(from, to, walkToDestiny);
    }

    private void walkingToTransfer() {
        LatLng toNewBus = new LatLng(Double.parseDouble(indirecto.getLinea().getRecorrido().get(0).getLatitud()), Double.parseDouble(indirecto.getLinea().getRecorrido().get(0).getLongitud()));
        LatLng from = latLngs.get(0);
        drawPolylineWalking(from, toNewBus, walkToStopBus);
    }

    private void walkingToDestinyFromTransfer() {
        LatLng fromNewBus = new LatLng(Double.parseDouble(indirecto.getLineaTransbordo().getRecorridoTransbordo().get(indirecto.getLineaTransbordo().getRecorridoTransbordo().size() - 1).getLatitud()), Double.parseDouble(indirecto.getLineaTransbordo().getRecorridoTransbordo().get(indirecto.getLineaTransbordo().getRecorridoTransbordo().size() - 1).getLongitud()));
        LatLng to = latLngs.get(1);
        drawPolylineWalking(fromNewBus, to, walkToDestiny);
    }

    private void setTimeArrival() {
        @SuppressLint("SimpleDateFormat")
        DateFormat df = new SimpleDateFormat("h:mm a");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, durationTotal);
        timeArrival.setText(df.format(calendar.getTime()));
    }


    private void addMarkerEndTrip() {
        markerDestiny = mMap.addMarker(new MarkerOptions()
                .position(latLngTo)
                .title("Destino")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        markerDestiny.showInfoWindow();
    }

    private void addMarkerWalk() {
        Drawable circleDrawable = getResources().getDrawable(R.drawable.ic_marker_walk);
        BitmapDescriptor markerIcon = getMarkerIconFromDrawable(circleDrawable);
        mMap.addMarker(new MarkerOptions().position(latLngFrom).icon(markerIcon).anchor(0.5f, 0.5f));
    }

    private void loadDirectUI() {
        numberBusDirect.setText(directo.getLinea().getLinea());
        takeBus.setText(getAddress(this, Double.parseDouble(directo.getLinea().getRecorrido().get(0).getLatitud()), Double.parseDouble(directo.getLinea().getRecorrido().get(0).getLongitud())));
        getOffBus.setText(getAddress(this, Double.parseDouble(directo.getLinea().getRecorrido().get(directo.getLinea().getRecorrido().size() - 1).getLatitud()), Double.parseDouble(directo.getLinea().getRecorrido().get(directo.getLinea().getRecorrido().size() - 1).getLongitud())));

        timeInitTravel.setText(DateUtil.getDate());
        walkingToGetBus();
        walkingToDestiny();
    }

    private void loadIndirectUI() {
        containerTakeAnotherBus.setVisibility(View.VISIBLE);
        container_walk_another_stop_bus.setVisibility(View.VISIBLE);
        numberBusDirect.setText(indirecto.getLinea().getLinea());
        numberBusIndirect.setText(indirecto.getLineaTransbordo().getLinea());
        takeBus.setText(getAddress(this, Double.parseDouble(indirecto.getLinea().getRecorrido().get(0).getLatitud()), Double.parseDouble(indirecto.getLinea().getRecorrido().get(0).getLongitud())));
        getOffBus.setText(getAddress(this, Double.parseDouble(indirecto.getLinea().getRecorrido().get(indirecto.getLinea().getRecorrido().size() - 1).getLatitud()), Double.parseDouble(indirecto.getLinea().getRecorrido().get(indirecto.getLinea().getRecorrido().size() - 1).getLongitud())));
        getOffAnotherBus.setText(getAddress(this, Double.parseDouble(indirecto.getLineaTransbordo().getRecorridoTransbordo().get(indirecto.getLineaTransbordo().getRecorridoTransbordo().size() - 1).getLatitud()), Double.parseDouble(indirecto.getLineaTransbordo().getRecorridoTransbordo().get(indirecto.getLineaTransbordo().getRecorridoTransbordo().size() - 1).getLongitud())));
        takeAnotherBus.setText(getAddress(this, Double.parseDouble(indirecto.getLineaTransbordo().getRecorridoTransbordo().get(0).getLatitud()), Double.parseDouble(indirecto.getLineaTransbordo().getRecorridoTransbordo().get(0).getLongitud())));

        timeInitTravel.setText(DateUtil.getDate());

        walkingToTransfer();
        walkingToDestinyFromTransfer();
    }

    private void drawPoly() {
        int color = getResources().getColor(R.color.colorPrimary);
        int color2 = getResources().getColor(R.color.colorAccent);
        List<LatLng> points = new ArrayList<>();
        for (int j = 0; j < indirecto.getLinea().getRecorrido().size(); j++) {
            points.add(new LatLng(Double.parseDouble(indirecto.getLinea().getRecorrido().get(j).getLatitud()), Double.parseDouble(indirecto.getLinea().getRecorrido().get(j).getLongitud())));
        }
        drawPolylineDirectBus(points, color);

        if (indirecto.getLineaTransbordo() != null) {
            List<LatLng> points2 = new ArrayList<>();
            for (int j = 0; j < indirecto.getLineaTransbordo().getRecorridoTransbordo().size(); j++) {
                points2.add(new LatLng(Double.parseDouble(indirecto.getLineaTransbordo().getRecorridoTransbordo().get(j).getLatitud()), Double.parseDouble(indirecto.getLineaTransbordo().getRecorridoTransbordo().get(j).getLongitud())));
            }
            drawPolylineDirectBus(points2, color2);
            drawPolylineWalking(points.get(points.size() - 1), points2.get(0), walkToBusTransfer);
        } else {
            AlerterHelper.showInfo(this, "No se encontraron lineas de transbordo");
        }
    }

    private void drawPolyDirect() {
        List<LatLng> points = new ArrayList<>();
        for (int j = 0; j < directo.getLinea().getRecorrido().size(); j++) {
            points.add(new LatLng(Double.parseDouble(directo.getLinea().getRecorrido().get(j).getLatitud()), Double.parseDouble(directo.getLinea().getRecorrido().get(j).getLongitud())));
        }
        int color = getResources().getColor(R.color.colorPrimary);
        drawPolylineDirectBus(points, color);
    }
}
