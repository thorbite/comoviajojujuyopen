package com.jujuy.cmoviajojujuy.view.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.View;
import android.widget.Toast;

import com.droidnet.DroidListener;
import com.droidnet.DroidNet;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.jujuy.cmoviajojujuy.BuildConfig;
import com.jujuy.cmoviajojujuy.R;
import com.jujuy.cmoviajojujuy.utils.BaseActivity;

import java.util.List;

public class SplashScreenActivity extends AppCompatActivity implements DroidListener {
    private static final int SPLASH_TIME_OUT = 2000;
    private boolean isConnected;
    private FusedLocationProviderClient mFusedLocationClient;
    private Location mLastLocation;
    private DroidNet mDroidNet;
    private LocationManager locationManager;
    public boolean GpsStatus = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CheckGpsStatus();
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mDroidNet = DroidNet.getInstance();
        mDroidNet.addInternetConnectivityListener(this);
    }

    private void requestPermissionsCheck() {
        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                if (isConnected) {
                    if (GpsStatus) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                getLastLocation();
                            }
                        }, SPLASH_TIME_OUT);
                    } else {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                goToMainActivity();
                            }
                        }, SPLASH_TIME_OUT);
                    }
                } else buildDialog(SplashScreenActivity.this).show();
            }

            @Override
            public void onPermissionDenied(List<String> deniedPermissions) {
                finish();
            }
        };

        TedPermission.with(this)
                .setPermissionListener(permissionlistener)
                .setDeniedTitle(R.string.permission_denied)
                .setDeniedMessage(R.string.permission_denied_explanation)
                .setGotoSettingButtonText(R.string.settings)
                .setDeniedCloseButtonText(R.string.close)
                .setPermissions(Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION)
                .check();
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        this.isConnected = isConnected;
        if (!checkPermissions()) {
            requestPermissionsCheck();
        } else if (isConnected) {
            if (checkPermissions() && GpsStatus) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getLastLocation();
                    }
                }, SPLASH_TIME_OUT);
            } else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        goToMainActivity();
                    }
                }, SPLASH_TIME_OUT);
            }

        } else {
            buildDialog(this).show();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
            /*if (checkPermissions() && GpsStatus) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getLastLocation();
                    }
                }, SPLASH_TIME_OUT);
            }*/
    }

    @SuppressWarnings("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient.getLastLocation()
                .addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            mLastLocation = task.getResult();
                            Intent i = new Intent(SplashScreenActivity.this, MainActivity.class);
                            i.putExtra("mLastLocation", mLastLocation);
                            startActivity(i);
                            finish();
                        }
                    }
                });
    }


    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    public AlertDialog.Builder buildDialog(Context c) {
        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setTitle("No estas conectado a Internet");
        builder.setCancelable(false);
        builder.setMessage("Necesitas activar Wifi o datos moviles para que la aplicacion funcione correctamente.");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        return builder;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mDroidNet.removeInternetConnectivityChangeListener(this);
    }

    public void CheckGpsStatus() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (locationManager != null) {
            GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } else CheckGpsStatus();
    }

    private void goToMainActivity(){
        Intent i = new Intent(SplashScreenActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }
}
