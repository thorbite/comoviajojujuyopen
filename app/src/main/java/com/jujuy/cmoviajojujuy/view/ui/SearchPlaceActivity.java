package com.jujuy.cmoviajojujuy.view.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.droidnet.DroidListener;
import com.droidnet.DroidNet;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.material.snackbar.Snackbar;
import com.jujuy.cmoviajojujuy.R;
import com.jujuy.cmoviajojujuy.view.adapter.PlacesAutoCompleteAdapter;

public class SearchPlaceActivity extends AppCompatActivity implements PlacesAutoCompleteAdapter.ClickListener, DroidListener {
    private static final String TAG = "SearchPlaceActivity";
    private RecyclerView searchResultsRV;
    private PlacesAutoCompleteAdapter mAutoCompleteAdapter;
    private ImageButton backImage;
    private ImageView eraseEntry, imageCurrentLocation;
    private EditText placeNameET;
    private TextView txtCurrentLocation;
    private LinearLayout viewCurrentPlace;
    private FusedLocationProviderClient mFusedLocationClient;
    private Location mLastLocation;
    private CardView offlineResultView;
    private boolean isConnected;
    private DroidNet mDroidNet;
    private LocationManager locationManager;
    private boolean GpsStatus = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_place);
        CheckGpsStatus();
        inflateViews();
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        if(!GpsStatus){
            imageCurrentLocation.setColorFilter(ContextCompat.getColor(SearchPlaceActivity.this, R.color.grayLight));
            txtCurrentLocation.setTextColor(getResources().getColor(R.color.grayLight));
        }
        viewCurrentPlace.setOnClickListener(view -> setCurrentLocationToEdt());
        backImage.setOnClickListener(view -> onBackPressed());
        eraseEntry.setOnClickListener(view -> eraseCurrentEntry());

        initPlaceApiKey();
        mAutoCompleteAdapter = new PlacesAutoCompleteAdapter(this);
        searchResultsRV.setLayoutManager(new LinearLayoutManager(this));
        mAutoCompleteAdapter.setClickListener(this);
        searchResultsRV.setAdapter(mAutoCompleteAdapter);
        mAutoCompleteAdapter.notifyDataSetChanged();

        mDroidNet = DroidNet.getInstance();
        mDroidNet.addInternetConnectivityListener(this);
    }

    private void inflateViews(){
        backImage = findViewById(R.id.backImageBtn);
        eraseEntry = findViewById(R.id.eraseCurrentEntryIV);
        placeNameET = findViewById(R.id.placeNamET);
        viewCurrentPlace = findViewById(R.id.location_view);
        imageCurrentLocation = findViewById(R.id.image_current_location);
        txtCurrentLocation = findViewById(R.id.txt_current_location);
        searchResultsRV = findViewById(R.id.searchResultsRV);
        offlineResultView = findViewById(R.id.offlineResultView);
        ((EditText) findViewById(R.id.placeNamET)).addTextChangedListener(filterTextWatcher);
    }

    private void initPlaceApiKey() {
        String apiKey = getString(R.string.google_maps_key);

        if (apiKey.equals("")) {
            Toast.makeText(this, "Error de la Api key", Toast.LENGTH_LONG).show();
            return;
        }

        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), apiKey);
        }
    }

    private void setCurrentLocationToEdt() {
        if (GpsStatus) {
            getLastLocation();
        }else {
            Toast.makeText(this, "Debes habilitar tu gps para usar esta funcion", Toast.LENGTH_SHORT).show();
        }
    }

    private void eraseCurrentEntry() {
        placeNameET.setText("");
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        this.isConnected = isConnected;
        if (!isConnected ) {
            if(offlineResultView.getVisibility() == View.GONE){
                offlineResultView.setVisibility(View.VISIBLE);
                viewCurrentPlace.setVisibility(View.GONE);
                if (searchResultsRV.getVisibility() == View.VISIBLE) {
                    searchResultsRV.setVisibility(View.GONE);
                }
            }
        } else {
                if (offlineResultView.getVisibility() == View.VISIBLE) {
                    offlineResultView.setVisibility(View.GONE);
                    viewCurrentPlace.setVisibility(View.VISIBLE);
                }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    private TextWatcher filterTextWatcher = new TextWatcher() {
        public void afterTextChanged(Editable s) {
            if (!s.toString().equals("")) {
                eraseEntry.setVisibility(View.VISIBLE);
                viewCurrentPlace.setVisibility(View.GONE);
                mAutoCompleteAdapter.getFilter().filter(s.toString());
                mAutoCompleteAdapter.notifyDataSetChanged();
                if (searchResultsRV.getVisibility() == View.GONE) {
                    searchResultsRV.setVisibility(View.VISIBLE);
                }
            } else {
                eraseEntry.setVisibility(View.GONE);
                if(isConnected){
                    viewCurrentPlace.setVisibility(View.VISIBLE);
                }
                if (searchResultsRV.getVisibility() == View.VISIBLE) {
                    searchResultsRV.setVisibility(View.GONE);
                }
            }

            if(!isConnected){
                searchResultsRV.setVisibility(View.GONE);
                offlineResultView.setVisibility(View.VISIBLE);
            }
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    };

    @Override
    public void click(Place place) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("place", place);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    @SuppressWarnings("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient.getLastLocation()
                .addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            mLastLocation = task.getResult();
                            Intent returnIntent = new Intent();
                            returnIntent.putExtra("mLastLocation", mLastLocation);
                            setResult(Activity.RESULT_OK, returnIntent);
                            finish();
                        } else {
                            showSnackbar(getString(R.string.no_location_detected));
                        }
                    }
                });
    }

    private void showSnackbar(final String text) {
        View container = findViewById(android.R.id.content);
        if (container != null) {
            Snackbar.make(container, text, Snackbar.LENGTH_LONG).show();
        }
    }

    public void CheckGpsStatus() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if(locationManager!=null){
            GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        }else CheckGpsStatus();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mDroidNet.removeInternetConnectivityChangeListener(this);
    }
}
