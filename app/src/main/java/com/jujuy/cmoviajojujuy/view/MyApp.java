package com.jujuy.cmoviajojujuy.view;

import android.app.Application;
import android.content.Context;

import com.droidnet.DroidNet;

public class MyApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        DroidNet.init(this);

    }

    public Context getContext(){
        return getBaseContext();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        DroidNet.getInstance().removeAllInternetConnectivityChangeListeners();
    }
}
