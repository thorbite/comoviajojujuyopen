package com.jujuy.cmoviajojujuy.utils;

import java.io.IOException;

public class NoConnectivityException extends IOException {

    @Override
    public String getMessage() {
        return "No estas conectado a internet";
        // You can send any message whatever you want from here.
    }
}
