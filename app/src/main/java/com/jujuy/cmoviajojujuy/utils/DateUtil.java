package com.jujuy.cmoviajojujuy.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DateUtil {
    public static String getDate() {
        DateFormat df = new SimpleDateFormat("h:mm a");
        return df.format(Calendar.getInstance().getTime());
    }
}
