package com.jujuy.cmoviajojujuy.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.droidnet.DroidListener;
import com.droidnet.DroidNet;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.jujuy.cmoviajojujuy.R;
import com.jujuy.cmoviajojujuy.view.ui.ProgressDialogFragment;

public class BaseActivity extends AppCompatActivity implements DroidListener {
    private LocationManager locationManager;
    public boolean GpsStatus=false;
    private FusedLocationProviderClient mFusedLocationClient;
    private DroidNet mDroidNet;
    public boolean isConnected;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CheckGpsStatus();
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mDroidNet = DroidNet.getInstance();
        mDroidNet.addInternetConnectivityListener(this);
      }

    public void initProgressBar() {
        ProgressDialogFragment.newInstance("I AM TITLE!!").show(getSupportFragmentManager(), "ProgressDialog");
    }

    public void dismissProgressBar() {
        FragmentManager manager = getSupportFragmentManager();
        Fragment prev = manager.findFragmentByTag("ProgressDialog");
        if (prev != null) {
            manager.beginTransaction().remove(prev).commit();
        }
    }

    public void CheckGpsStatus() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if(locationManager!=null){
            GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        }else CheckGpsStatus();
    }

    public AlertDialog.Builder buildDialog(Context c) {

        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setTitle("No estas conectado a Internet");
        builder.setCancelable(false);
        builder.setMessage("Necesitas activar Wifi o datos moviles para accesar. Presione OK para salir");

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        return builder;
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        this.isConnected = isConnected;
        if (isConnected) {
            netIsOn();
        } else {
            netIsOff();
        }
    }

    private void netIsOn(){

    }

    private void netIsOff(){
        //buildDialog(this).show();
        Toast.makeText(this, "No tienes conexion a internet", Toast.LENGTH_LONG).show();
    }
}
