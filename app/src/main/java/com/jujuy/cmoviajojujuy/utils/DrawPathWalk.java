package com.jujuy.cmoviajojujuy.utils;

import android.graphics.Color;
import android.os.AsyncTask;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Dash;
import com.google.android.gms.maps.model.Dot;
import com.google.android.gms.maps.model.Gap;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;

import java.util.Arrays;
import java.util.List;

public class DrawPathWalk extends AsyncTask {
    public static final int PATTERN_DASH_LENGTH_PX = 5;
    public static final int PATTERN_GAP_LENGTH_PX = 5;
    public static final PatternItem DOT = new Dot();
    public static final PatternItem DASH = new Dash(PATTERN_DASH_LENGTH_PX);
    public static final PatternItem GAP = new Gap(PATTERN_GAP_LENGTH_PX);
    public static final List<PatternItem> PATTERN_DOTTED_LINE = Arrays.asList(DOT,GAP);

    private static String[] polyline;
    private static GoogleMap mMap;
    private static int color;

    public DrawPathWalk(GoogleMap map, String[] polyline, int color){
        this.mMap = map;
        this.polyline = polyline;
        this.color = color;
    }

    @Override
    protected Object doInBackground (Object[] params) {
        PolylineOptions options = new PolylineOptions();
        options.color(color);
        options.width(15);
        options.geodesic(true);
        options.pattern(PATTERN_DOTTED_LINE);
        for (String polyline : polyline) {
            options.addAll(PolyUtil.decode(polyline));
        }
        return options;
    }

    protected void onPostExecute(Object result) {
        mMap.addPolyline((PolylineOptions) result);
    }
}