package com.jujuy.cmoviajojujuy.utils;
import android.os.AsyncTask;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;

public class DrawPathBus extends AsyncTask {

    private static String[] polyline;
    private static GoogleMap mMap;
    private static int color;

    public DrawPathBus(GoogleMap map, String[] polyline,int color){
        this.mMap = map;
        this.polyline = polyline;
        this.color = color;
    }

    @Override
    protected Object doInBackground (Object[] params) {
        PolylineOptions options = new PolylineOptions();
        options.color(color);
        options.width(10);
        options.geodesic(true);
        for (String s : polyline) {
            options.addAll(PolyUtil.decode(s));
        }
        return options;
    }

    protected void onPostExecute(Object result) {
        mMap.addPolyline((PolylineOptions) result);
    }
}