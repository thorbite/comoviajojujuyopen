package com.jujuy.cmoviajojujuy.utils;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import com.jujuy.cmoviajojujuy.services.modelApiGoogle.Direction;
import com.jujuy.cmoviajojujuy.services.modelApiGoogle.Step;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class DirectionsHelper {
    public static String getAddress(Context context, double LATITUDE, double LONGITUDE) {
        String address = "";
        //Set Address
        try {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null && addresses.size() > 0) {
                String thoroughfare = addresses.get(0).getThoroughfare();
                String number = addresses.get(0).getFeatureName(); // Only if available else return NULL

                if (thoroughfare != null)
                    address = addresses.get(0).getThoroughfare();
                if (number != null) {
                    if (address.equals(""))
                        address = addresses.get(0).getFeatureName();
                    else
                        address += ", " + addresses.get(0).getFeatureName();
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return address;
    }

    public static String[] getPolylineWalkToStopBus(Direction direction) {
        String[] polylineArray = null;

        if (direction != null && direction.getStatus().equals("OK")) {
            int count = direction.getRoutes().get(0).getLegs().get(0).getSteps().size();
            Step[] steps = direction.getRoutes().get(0).getLegs().get(0).getSteps().toArray(new Step[count]);
            polylineArray = new String[count];
            for (int i = 0; i < count; i++) {
                polylineArray[i] = steps[i].getPolyline().getPoints();
            }
        }
        return polylineArray;
    }



}
