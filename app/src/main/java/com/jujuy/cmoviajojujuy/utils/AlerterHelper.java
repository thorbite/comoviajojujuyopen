package com.jujuy.cmoviajojujuy.utils;

import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;

import com.jujuy.cmoviajojujuy.R;
import com.tapadoo.alerter.Alerter;

public class AlerterHelper {
    public static void showError(Context context, String message){
        Alerter.create((AppCompatActivity)context)
                .setTitle(R.string.error)
                .setText(message)
                .setDuration(4000)
                .setIcon(R.drawable.ic_error)
                .setBackgroundColorRes(R.color.accent_red)
                .show();
    }

    public static void showInfo(Context context, String message){
        Alerter.create((AppCompatActivity)context)
                .setText(message)
                .setIcon(R.drawable.ic_info)
                .setBackgroundColorRes(R.color.accent_cyan)
                .show();
    }
}
