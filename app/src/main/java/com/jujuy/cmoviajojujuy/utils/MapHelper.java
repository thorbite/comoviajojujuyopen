package com.jujuy.cmoviajojujuy.utils;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.List;

public class MapHelper {

        public static void centerLatLngsInMap(GoogleMap googleMap, List<LatLng> locations, boolean animate) {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (LatLng location : locations) {
                builder.include(location);
            }
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(builder.build(), 100);
            if (animate) {
                googleMap.animateCamera(cu);
            } else {
                googleMap.moveCamera(cu);
            }
        }

    }

