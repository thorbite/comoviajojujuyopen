
package com.jujuy.cmoviajojujuy.services.modelProyect;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Directo implements Serializable
{

    @SerializedName("linea")
    @Expose
    private Linea linea;
    private final static long serialVersionUID = -6612834301472935431L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Directo() {
    }

    /**
     * 
     * @param linea
     */
    public Directo(Linea linea) {
        super();
        this.linea = linea;
    }

    public Linea getLinea() {
        return linea;
    }

    public void setLinea(Linea linea) {
        this.linea = linea;
    }

}
