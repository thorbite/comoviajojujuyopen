
package com.jujuy.cmoviajojujuy.services.modelProyect;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Recorrido implements Serializable
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("nombre")
    @Expose
    private String nombre;
    @SerializedName("latitud")
    @Expose
    private String latitud;
    @SerializedName("longitud")
    @Expose
    private String longitud;
    @SerializedName("id_linea")
    @Expose
    private String idLinea;
    private final static long serialVersionUID = 2302641316603180068L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Recorrido() {
    }

    /**
     * 
     * @param idLinea
     * @param latitud
     * @param longitud
     * @param id
     * @param nombre
     */
    public Recorrido(String id, String nombre, String latitud, String longitud, String idLinea) {
        super();
        this.id = id;
        this.nombre = nombre;
        this.latitud = latitud;
        this.longitud = longitud;
        this.idLinea = idLinea;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getIdLinea() {
        return idLinea;
    }

    public void setIdLinea(String idLinea) {
        this.idLinea = idLinea;
    }

}
