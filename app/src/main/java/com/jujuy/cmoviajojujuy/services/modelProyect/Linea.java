
package com.jujuy.cmoviajojujuy.services.modelProyect;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Linea implements Serializable
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("Parada")
    @Expose
    private String parada;
    @SerializedName("latitud")
    @Expose
    private String latitud;
    @SerializedName("longitud")
    @Expose
    private String longitud;
    @SerializedName("id_linea")
    @Expose
    private String idLinea;
    @SerializedName("distancia")
    @Expose
    private String distancia;
    @SerializedName("Linea")
    @Expose
    private String linea;
    @SerializedName("Linea Descripcion")
    @Expose
    private String lineaDescripcion;
    @SerializedName("id_empresa")
    @Expose
    private String idEmpresa;
    @SerializedName("Empresa")
    @Expose
    private String empresa;
    @SerializedName("recorrido")
    @Expose
    private List<Recorrido> recorrido = null;
    private final static long serialVersionUID = 5237736395552209296L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Linea() {
    }

    /**
     * 
     * @param idLinea
     * @param latitud
     * @param longitud
     * @param parada
     * @param distancia
     * @param idEmpresa
     * @param id
     * @param recorrido
     * @param empresa
     * @param lineaDescripcion
     * @param linea
     */
    public Linea(String id, String parada, String latitud, String longitud, String idLinea, String distancia, String linea, String lineaDescripcion, String idEmpresa, String empresa, List<Recorrido> recorrido) {
        super();
        this.id = id;
        this.parada = parada;
        this.latitud = latitud;
        this.longitud = longitud;
        this.idLinea = idLinea;
        this.distancia = distancia;
        this.linea = linea;
        this.lineaDescripcion = lineaDescripcion;
        this.idEmpresa = idEmpresa;
        this.empresa = empresa;
        this.recorrido = recorrido;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParada() {
        return parada;
    }

    public void setParada(String parada) {
        this.parada = parada;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getIdLinea() {
        return idLinea;
    }

    public void setIdLinea(String idLinea) {
        this.idLinea = idLinea;
    }

    public String getDistancia() {
        return distancia;
    }

    public void setDistancia(String distancia) {
        this.distancia = distancia;
    }

    public String getLinea() {
        return linea;
    }

    public void setLinea(String linea) {
        this.linea = linea;
    }

    public String getLineaDescripcion() {
        return lineaDescripcion;
    }

    public void setLineaDescripcion(String lineaDescripcion) {
        this.lineaDescripcion = lineaDescripcion;
    }

    public String getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(String idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public List<Recorrido> getRecorrido() {
        return recorrido;
    }

    public void setRecorrido(List<Recorrido> recorrido) {
        this.recorrido = recorrido;
    }

}
