
package com.jujuy.cmoviajojujuy.services.modelProyect;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data implements Serializable
{

    @SerializedName("directos")
    @Expose
    private List<Directo> directos = null;
    @SerializedName("indirectos")
    @Expose
    private List<Object> indirectos = null;
    private final static long serialVersionUID = 4992852255665564776L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Data() {
    }

    /**
     * 
     * @param indirectos
     * @param directos
     */
    public Data(List<Directo> directos, List<Object> indirectos) {
        super();
        this.directos = directos;
        this.indirectos = indirectos;
    }

    public List<Directo> getDirectos() {
        return directos;
    }

    public void setDirectos(List<Directo> directos) {
        this.directos = directos;
    }

    public List<Object> getIndirectos() {
        return indirectos;
    }

    public void setIndirectos(List<Object> indirectos) {
        this.indirectos = indirectos;
    }

}
