
package com.jujuy.cmoviajojujuy.services.modelPublicidades;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Publicidade implements Serializable
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("titulo_publicidad")
    @Expose
    private String tituloPublicidad;
    @SerializedName("descripcion_publicidad")
    @Expose
    private String descripcionPublicidad;
    @SerializedName("nombre_archivo")
    @Expose
    private String nombreArchivo;
    @SerializedName("ruta_archivo")
    @Expose
    private String rutaArchivo;
    @SerializedName("tipo_archivo")
    @Expose
    private String tipoArchivo;
    @SerializedName("duracion_publicidad")
    @Expose
    private Integer duracionPublicidad;
    @SerializedName("tipo_publicidad")
    @Expose
    private Boolean tipoPublicidad;
    @SerializedName("estado")
    @Expose
    private Boolean estado;
    private final static long serialVersionUID = 2012555027035979073L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Publicidade() {
    }

    /**
     * 
     * @param nombreArchivo
     * @param rutaArchivo
     * @param estado
     * @param tituloPublicidad
     * @param tipoPublicidad
     * @param descripcionPublicidad
     * @param duracionPublicidad
     * @param id
     * @param tipoArchivo
     */
    public Publicidade(String id, String tituloPublicidad, String descripcionPublicidad, String nombreArchivo, String rutaArchivo, String tipoArchivo, Integer duracionPublicidad, Boolean tipoPublicidad, Boolean estado) {
        super();
        this.id = id;
        this.tituloPublicidad = tituloPublicidad;
        this.descripcionPublicidad = descripcionPublicidad;
        this.nombreArchivo = nombreArchivo;
        this.rutaArchivo = rutaArchivo;
        this.tipoArchivo = tipoArchivo;
        this.duracionPublicidad = duracionPublicidad;
        this.tipoPublicidad = tipoPublicidad;
        this.estado = estado;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTituloPublicidad() {
        return tituloPublicidad;
    }

    public void setTituloPublicidad(String tituloPublicidad) {
        this.tituloPublicidad = tituloPublicidad;
    }

    public String getDescripcionPublicidad() {
        return descripcionPublicidad;
    }

    public void setDescripcionPublicidad(String descripcionPublicidad) {
        this.descripcionPublicidad = descripcionPublicidad;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getRutaArchivo() {
        return rutaArchivo;
    }

    public void setRutaArchivo(String rutaArchivo) {
        this.rutaArchivo = rutaArchivo;
    }

    public String getTipoArchivo() {
        return tipoArchivo;
    }

    public void setTipoArchivo(String tipoArchivo) {
        this.tipoArchivo = tipoArchivo;
    }

    public Integer getDuracionPublicidad() {
        return duracionPublicidad;
    }

    public void setDuracionPublicidad(Integer duracionPublicidad) {
        this.duracionPublicidad = duracionPublicidad;
    }

    public Boolean getTipoPublicidad() {
        return tipoPublicidad;
    }

    public void setTipoPublicidad(Boolean tipoPublicidad) {
        this.tipoPublicidad = tipoPublicidad;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

}
