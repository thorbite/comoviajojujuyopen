
package com.jujuy.cmoviajojujuy.services.modelProyect;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LineaTransbordo implements Serializable
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("Parada")
    @Expose
    private String parada;
    @SerializedName("latitud")
    @Expose
    private String latitud;
    @SerializedName("longitud")
    @Expose
    private String longitud;
    @SerializedName("id_linea")
    @Expose
    private String idLinea;
    @SerializedName("distancia")
    @Expose
    private String distancia;
    @SerializedName("Linea")
    @Expose
    private String linea;
    @SerializedName("Linea Descripcion")
    @Expose
    private String lineaDescripcion;
    @SerializedName("id_empresa")
    @Expose
    private String idEmpresa;
    @SerializedName("Empresa")
    @Expose
    private String empresa;
    @SerializedName("recorrido_transbordo")
    @Expose
    private List<RecorridoTransbordo> recorridoTransbordo = null;
    private final static long serialVersionUID = 1742216802293040716L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public LineaTransbordo() {
    }

    /**
     * 
     * @param idLinea
     * @param latitud
     * @param longitud
     * @param recorridoTransbordo
     * @param parada
     * @param distancia
     * @param idEmpresa
     * @param id
     * @param empresa
     * @param lineaDescripcion
     * @param linea
     */
    public LineaTransbordo(String id, String parada, String latitud, String longitud, String idLinea, String distancia, String linea, String lineaDescripcion, String idEmpresa, String empresa, List<RecorridoTransbordo> recorridoTransbordo) {
        super();
        this.id = id;
        this.parada = parada;
        this.latitud = latitud;
        this.longitud = longitud;
        this.idLinea = idLinea;
        this.distancia = distancia;
        this.linea = linea;
        this.lineaDescripcion = lineaDescripcion;
        this.idEmpresa = idEmpresa;
        this.empresa = empresa;
        this.recorridoTransbordo = recorridoTransbordo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParada() {
        return parada;
    }

    public void setParada(String parada) {
        this.parada = parada;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getIdLinea() {
        return idLinea;
    }

    public void setIdLinea(String idLinea) {
        this.idLinea = idLinea;
    }

    public String getDistancia() {
        return distancia;
    }

    public void setDistancia(String distancia) {
        this.distancia = distancia;
    }

    public String getLinea() {
        return linea;
    }

    public void setLinea(String linea) {
        this.linea = linea;
    }

    public String getLineaDescripcion() {
        return lineaDescripcion;
    }

    public void setLineaDescripcion(String lineaDescripcion) {
        this.lineaDescripcion = lineaDescripcion;
    }

    public String getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(String idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public List<RecorridoTransbordo> getRecorridoTransbordo() {
        return recorridoTransbordo;
    }

    public void setRecorridoTransbordo(List<RecorridoTransbordo> recorridoTransbordo) {
        this.recorridoTransbordo = recorridoTransbordo;
    }

}
