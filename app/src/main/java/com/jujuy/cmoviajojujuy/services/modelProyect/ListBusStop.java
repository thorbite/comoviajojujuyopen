
package com.jujuy.cmoviajojujuy.services.modelProyect;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListBusStop implements Serializable
{

    @SerializedName("directos")
    @Expose
    private List<Directo> directBusStops = null;
    @SerializedName("indirectos")
    @Expose
    private List<Indirecto> IndirectbusStops = null;
    private final static long serialVersionUID = 6696621254022418091L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ListBusStop() {
    }

    /**
     * 
     * @param busStops
     * @param directBusStops
     */
    public ListBusStop(List<Directo> directBusStops, List<Indirecto> busStops) {
        super();
        this.directBusStops = directBusStops;
        this.IndirectbusStops = busStops;
    }

    public List<Directo> getDirectBusStops() {
        return directBusStops;
    }

    public void setDirectBusStops(List<Directo> directBusStops) {
        this.directBusStops = directBusStops;
    }

    public List<Indirecto> getIndirectBusStops() {
        return IndirectbusStops;
    }

    public void setIndirectBusStops(List<Indirecto> busStops) {
        this.IndirectbusStops = busStops;
    }

}
