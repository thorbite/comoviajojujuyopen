
package com.jujuy.cmoviajojujuy.services.modelProyect;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Indirecto implements Serializable
{

    @SerializedName("linea")
    @Expose
    private Linea linea;
    @SerializedName("linea_transbordo")
    @Expose
    private LineaTransbordo lineaTransbordo;
    private final static long serialVersionUID = -5618225893776386791L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Indirecto() {
    }

    /**
     * 
     * @param linea
     * @param lineaTransbordo
     */
    public Indirecto(Linea linea, LineaTransbordo lineaTransbordo) {
        super();
        this.linea = linea;
        this.lineaTransbordo = lineaTransbordo;
    }

    public Linea getLinea() {
        return linea;
    }

    public void setLinea(Linea linea) {
        this.linea = linea;
    }

    public LineaTransbordo getLineaTransbordo() {
        return lineaTransbordo;
    }

    public void setLineaTransbordo(LineaTransbordo lineaTransbordo) {
        this.lineaTransbordo = lineaTransbordo;
    }

}
