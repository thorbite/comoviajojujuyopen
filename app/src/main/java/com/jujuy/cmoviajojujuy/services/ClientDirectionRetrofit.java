package com.jujuy.cmoviajojujuy.services;

import android.content.Context;

import com.jujuy.cmoviajojujuy.utils.NetworkConnectionInterceptor;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ClientDirectionRetrofit {
    private static Retrofit client;

    public static Retrofit getInstance(String baseUrl){

            client = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        return client;
    }
}
