package com.jujuy.cmoviajojujuy.services.modelProyect;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RequestBusStop implements Serializable {

    @SerializedName("limite")
    @Expose
    private Integer limite;
    @SerializedName("radio")
    @Expose
    private Integer radio;
    @SerializedName("longitudDesde")
    @Expose
    private String longitudDesde;
    @SerializedName("latitudDesde")
    @Expose
    private String latitudDesde;
    @SerializedName("longitudHasta")
    @Expose
    private String longitudHasta;
    @SerializedName("latitudHasta")
    @Expose
    private String latitudHasta;
    private final static long serialVersionUID = 790560150954959990L;

    /**
     * No args constructor for use in serialization
     *
     */
    public RequestBusStop() {
    }

    /**
     *
     * @param latitudHasta
     * @param longitudDesde
     * @param longitudHasta
     * @param limite
     * @param latitudDesde
     * @param radio
     */
    public RequestBusStop(String latitudDesde, String longitudDesde, String latitudHasta, String longitudHasta, Integer limite, Integer radio) {
        super();
        this.limite = limite;
        this.radio = radio;
        this.longitudDesde = longitudDesde;
        this.latitudDesde = latitudDesde;
        this.longitudHasta = longitudHasta;
        this.latitudHasta = latitudHasta;
    }

    public Integer getLimite() {
        return limite;
    }

    public void setLimite(Integer limite) {
        this.limite = limite;
    }

    public Integer getRadio() {
        return radio;
    }

    public void setRadio(Integer radio) {
        this.radio = radio;
    }

    public String getLongitudDesde() {
        return longitudDesde;
    }

    public void setLongitudDesde(String longitudDesde) {
        this.longitudDesde = longitudDesde;
    }

    public String getLatitudDesde() {
        return latitudDesde;
    }

    public void setLatitudDesde(String latitudDesde) {
        this.latitudDesde = latitudDesde;
    }

    public String getLongitudHasta() {
        return longitudHasta;
    }

    public void setLongitudHasta(String longitudHasta) {
        this.longitudHasta = longitudHasta;
    }

    public String getLatitudHasta() {
        return latitudHasta;
    }

    public void setLatitudHasta(String latitudHasta) {
        this.latitudHasta = latitudHasta;
    }
}
