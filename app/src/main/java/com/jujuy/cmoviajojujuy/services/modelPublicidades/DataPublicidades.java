
package com.jujuy.cmoviajojujuy.services.modelPublicidades;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataPublicidades implements Serializable
{

    @SerializedName("publicidades")
    @Expose
    private List<Publicidade> publicidades = null;
    private final static long serialVersionUID = -5495313489229652798L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public DataPublicidades() {
    }

    /**
     * 
     * @param publicidades
     */
    public DataPublicidades(List<Publicidade> publicidades) {
        super();
        this.publicidades = publicidades;
    }

    public List<Publicidade> getPublicidades() {
        return publicidades;
    }

    public void setPublicidades(List<Publicidade> publicidades) {
        this.publicidades = publicidades;
    }

}
