
package com.jujuy.cmoviajojujuy.services.modelPublicidades;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jujuy.cmoviajojujuy.services.modelProyect.Data;

public class ResponseBusPublicidades implements Serializable
{

    @SerializedName("data")
    @Expose
    private DataPublicidades data;
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Object status;
    private final static long serialVersionUID = 4333135869511367153L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ResponseBusPublicidades() {
    }

    /**
     * 
     * @param code
     * @param data
     * @param message
     * @param status
     */
    public ResponseBusPublicidades(DataPublicidades data, Integer code, String message, Object status) {
        super();
        this.data = data;
        this.code = code;
        this.message = message;
        this.status = status;
    }

    public DataPublicidades getData() {
        return data;
    }

    public void setData(DataPublicidades data) {
        this.data = data;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

}
