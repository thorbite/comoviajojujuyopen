
package com.jujuy.cmoviajojujuy.services.modelProyect;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseBusStop implements Serializable
{

    @SerializedName("data")
    @Expose
    private ListBusStop listBusStop;
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Object status;
    private final static long serialVersionUID = 8640095182709053384L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ResponseBusStop() {
    }

    /**
     * 
     * @param code
     * @param listBusStop
     * @param message
     * @param status
     */
    public ResponseBusStop(ListBusStop listBusStop, Integer code, String message, Object status) {
        super();
        this.listBusStop = listBusStop;
        this.code = code;
        this.message = message;
        this.status = status;
    }

    public ListBusStop getListBusStop() {
        return listBusStop;
    }

    public void setListBusStop(ListBusStop listBusStop) {
        this.listBusStop = listBusStop;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

}
