package com.jujuy.cmoviajojujuy.repository;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.jujuy.cmoviajojujuy.services.ClientDirectionRetrofit;
import com.jujuy.cmoviajojujuy.services.modelProyect.RequestBusStop;
import com.jujuy.cmoviajojujuy.services.modelProyect.ResponseBusStop;
import com.jujuy.cmoviajojujuy.services.modelPublicidades.ResponseBusPublicidades;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ApiBusRepository {
    private static ApiBusRepository apiRepository;
    private BusServiceApi busServiceApi;
    private String baseUrl = "http://45.132.240.28/como-viajo/web/app_dev.php/api/public/";

    private ApiBusRepository(){
        Retrofit client = ClientDirectionRetrofit.getInstance(baseUrl);
        busServiceApi = client.create(BusServiceApi.class);
    }

    public synchronized static ApiBusRepository getInstance(){
        if(apiRepository == null){
            apiRepository = new ApiBusRepository();
        }
        return apiRepository;
    }

    public LiveData<ResponseBusStop> getSuggestionsStopBus(RequestBusStop requestBusStop){
        final MutableLiveData<ResponseBusStop> data = new MutableLiveData<>();
        busServiceApi.getSuggestionsStopBus(requestBusStop)
                .enqueue(new Callback<ResponseBusStop>() {
                    @Override
                    public void onResponse(Call<ResponseBusStop> call, Response<ResponseBusStop> response) {
                        if(response.isSuccessful()){
                            data.setValue(response.body());
                        }
                        else {
                            data.setValue(null);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBusStop> call, Throwable t) {
                        data.setValue(null);
                    }
                });
        return data;
    }

    public LiveData<ResponseBusPublicidades> getPublicidades(){
        final MutableLiveData<ResponseBusPublicidades> data = new MutableLiveData<>();
        busServiceApi.getBusPublicidades().enqueue(new Callback<ResponseBusPublicidades>() {
            @Override
            public void onResponse(Call<ResponseBusPublicidades> call, Response<ResponseBusPublicidades> response) {
                if(response.isSuccessful()){
                    data.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponseBusPublicidades> call, Throwable t) {
                data.setValue(null);
            }
        });
        return data;
    }
}
