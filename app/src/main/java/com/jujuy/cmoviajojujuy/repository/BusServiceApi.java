package com.jujuy.cmoviajojujuy.repository;

import com.jujuy.cmoviajojujuy.services.modelProyect.RequestBusStop;
import com.jujuy.cmoviajojujuy.services.modelProyect.ResponseBusStop;
import com.jujuy.cmoviajojujuy.services.modelPublicidades.ResponseBusPublicidades;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface BusServiceApi {
    @POST("parada/search")
    Call<ResponseBusStop> getSuggestionsStopBus (@Body RequestBusStop requestBusStop);

    @POST("publicidades")
    Call<ResponseBusPublicidades> getBusPublicidades();

}
