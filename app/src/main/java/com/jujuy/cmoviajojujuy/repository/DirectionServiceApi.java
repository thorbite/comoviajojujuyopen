package com.jujuy.cmoviajojujuy.repository;

import com.jujuy.cmoviajojujuy.services.modelApiGoogle.Direction;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface DirectionServiceApi {
    @GET("directions/json")
    Call<Direction> getDirectionTo(@Query("origin") String origin
            , @Query("destination") String destination
            , @Query("mode") String mode
            , @Query("key") String key);

    @GET("directions/json")
    Call<Direction> getWalkDirection(@Query("origin") String origin
            , @Query("destination") String destination
            , @Query("mode") String mode
            , @Query("key") String key);
}
