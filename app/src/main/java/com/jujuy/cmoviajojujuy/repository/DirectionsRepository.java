package com.jujuy.cmoviajojujuy.repository;


import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.jujuy.cmoviajojujuy.services.ClientDirectionRetrofit;
import com.jujuy.cmoviajojujuy.services.modelApiGoogle.Direction;
import com.jujuy.cmoviajojujuy.services.modelApiGoogle.Step;
import com.jujuy.cmoviajojujuy.services.modelProyect.Directo;
import com.jujuy.cmoviajojujuy.view.MyApp;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class DirectionsRepository {
    private static DirectionsRepository directionsRepository;
    private DirectionServiceApi directionServiceApi;
    private String baseUrl = "https://maps.googleapis.com/maps/api/";
    private DirectionsRepository(){
        Retrofit client = ClientDirectionRetrofit.getInstance(baseUrl);
        directionServiceApi = client.create(DirectionServiceApi.class);
    }

    public synchronized static DirectionsRepository getInstance(){
        if(directionsRepository == null){
            directionsRepository = new DirectionsRepository();
        }
        return directionsRepository;
    }
    public LiveData<Direction> getDirectionsTo(String origin, String destination, String mode,String key){
        final MutableLiveData<Direction> data = new MutableLiveData<>();
        directionServiceApi.getDirectionTo(origin,destination,mode,key)
                .enqueue(new Callback<Direction>() {
                    @Override
                    public void onResponse(@NonNull Call<Direction> call, @NonNull Response<Direction> response) {
                        if(response.isSuccessful()){
                            data.setValue(response.body());
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<Direction> call, @NonNull Throwable t) {
                        data.setValue(null);
                    }
                });
        return data;
    }

    public LiveData<String[]> getPolyline(Direction direction){
        MutableLiveData<String[]> polyline = new MutableLiveData<>();
        String[] polylineArray = null;

        if(direction != null && direction.getStatus().equals("OK")){
            int count = direction.getRoutes().get(0).getLegs().get(0).getSteps().size();
            Step[] steps = direction.getRoutes().get(0).getLegs().get(0).getSteps().toArray(new Step[count]);
            polylineArray = new String[count];
            for(int i=0;i<count;i++){
                polylineArray[i] = steps[i].getPolyline().getPoints();
            }
        }
        polyline.setValue(polylineArray);
        return polyline;
    }


    public LiveData<Direction> getWalkDirectionsTo(String origin, String destination, String mode,String key){
        final MutableLiveData<Direction> data = new MutableLiveData<>();
        directionServiceApi.getWalkDirection(origin,destination,mode,key)
                .enqueue(new Callback<Direction>() {
                    @Override
                    public void onResponse(@NonNull Call<Direction> call, @NonNull Response<Direction> response) {
                        if(response.isSuccessful()){
                            data.setValue(response.body());
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<Direction> call, @NonNull Throwable t) {
                        data.setValue(null);
                    }
                });
        return data;
    }


}
